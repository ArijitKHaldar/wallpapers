# wallpapers

A collection of wallpapers from different sources

Please note, I collected these images from the internet, and have no idea whom to give credits for their works of art.
If you find your image here, and you don't want people to use your images without proper credits or references to your work, then let me know.
I will remove any images that aren't meant to be freely re-distributed as soon as I get to know about it.

Sorry for my ignorance. Thank you for your consideration.

All wallpapers here are uploaded using Git-LFS.
1. Install Git LFS on your system
```
sudo pacman -S git-lfs
```
2. Initialize Git LFS
```
git lfs install
```

Now, git clone would automatically pull from LFS repositories

